<?php namespace Simonpioli\Chimp;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class ChimpServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('simonpioli/chimp');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		if(!Config::get('chimp.api_key') || Config::get('chimp.api_key') == 'your-key-here' || Config::get('chimp.api_key') == '') {
			throw new \Exception('Please supply a valid API key.');
		}


		$apikey = Config::get('chimp.api_key');

		$settings = Config::get('chimp.settings');

		$this->app->bind('chimp',  function() {
			return new Mailchimp($apikey, $settings);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('chimp');
	}

}