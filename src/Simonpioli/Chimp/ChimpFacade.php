<?php namespace Simonpioli\Chimp\Facades;

use Illuminate\Support\Facades\Facade;

class Chimp extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'chimp';
    }
}